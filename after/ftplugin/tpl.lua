vim.opt_local.tabstop = 2
vim.opt_local.shiftwidth = 2

-- vim.filetype.add({
-- 	extension = {
-- 		gotmpl = 'gotmpl',
-- 	},
-- 	pattern = {
-- 		["*.tpl"] = "tpl",
-- 		[".*/templates/.*%.tpl"] = "helm",
-- 		[".*/templates/.*%.ya?ml"] = "helm",
-- 		["helmfile.*%.ya?ml"] = "helm",
-- 	},
-- })
