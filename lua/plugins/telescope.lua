return {
  -- Fuzzy Finder (files, lsp, etc)
  {
    'nvim-telescope/telescope.nvim',
    branch = '0.1.x',
    dependencies = {
      'nvim-lua/plenary.nvim',
      -- Fuzzy Finder Algorithm which requires local dependencies to be built.
      -- Only load if `make` is available. Make sure you have the system
      -- requirements installed.
      {
        'stevearc/dressing.nvim',
        dependencies = { "nvim-telescope/telescope.nvim", "nvim-lua/plenary.nvim" },
        opts = {},
      },
      {
        "nvim-telescope/telescope-file-browser.nvim",
        dependencies = { "nvim-telescope/telescope.nvim", "nvim-lua/plenary.nvim" },
        config = function()
          vim.api.nvim_set_keymap(
            "n",
            "<leader>fb",
            ":Telescope file_browser<CR>",
            { noremap = true, desc = "[F]iles [b]rowser" }
          )
        end
      },
      {
        'nvim-telescope/telescope-fzf-native.nvim',
        -- NOTE: If you are having trouble with this installation,
        --       refer to the README for telescope-fzf-native for more instructions.
        build = 'make',
        cond = function()
          return vim.fn.executable 'make' == 1
        end,
      },
      {
        'edolphin-ydf/goimpl.nvim',
        dependencies = {
          { 'nvim-lua/popup.nvim' },
        },
        ft = "go",
        config = function()
          require 'telescope'.load_extension 'goimpl'

          local function impl()
            require 'telescope'.extensions.goimpl.goimpl {}
          end

          vim.keymap.set('n', '<leader>im', impl, { noremap = true, silent = true })
        end,
      },
      { 'jvgrootveld/telescope-zoxide' },
    },
    config = function()
      local z_utils = require("telescope._extensions.zoxide.utils")
      local fb_actions = require "telescope._extensions.file_browser.actions"
      local atc = function(prompt, default_value)
        local yes_values = { "y", "yes" }

        default_value = default_value or ""
        local confirmation = vim.fn.input(prompt .. " [y/yes, N/NO]: ", default_value)
        confirmation = string.lower(confirmation)
        if string.len(confirmation) == 0 then
          return false
        end

        for _, v in pairs(yes_values) do
          if v == confirmation then
            return true
          end
        end

        return false
      end

      local action_state = require "telescope.actions.state"
      local get_target_dir = function(finder)
        local entry_path
        if finder.files == false then
          local entry = action_state.get_selected_entry()
          entry_path = entry and entry.value -- absolute path
        end
        return finder.files and finder.path or entry_path
      end

      -- [[ Configure Telescope ]]
      -- See `:help telescope` and `:help telescope.setup()`
      require('telescope').setup {
        defaults = {
          mappings = {
            i = {
              ['<C-u>'] = false,
              ['<C-d>'] = "delete_buffer",
              ["<C-h>"] = "which_key"
            },
            n = {
              ['<C-d>'] = "delete_buffer",
              ["<C-h>"] = "which_key"
            },
          },
        },

        pickers = {
          registers = {
            mappings = {
              i = {
                ["<C-x>"] = function(prompt_bufnr)
                  local cp = action_state.get_current_picker(prompt_bufnr)
                  local selection = action_state.get_selected_entry()

                  if atc("Are you sure that you want to clear selected registers?") == false then
                    return
                  end

                  local selections = {}

                  if vim.tbl_isempty(cp:get_multi_selection()) then
                    table.insert(selections, selection)
                  else
                    for _, entry in ipairs(cp:get_multi_selection()) do
                      table.insert(selections, entry)
                    end
                  end

                  for _, v in pairs(selections) do
                    if v.content ~= "" then
                      v.content = ""
                      vim.fn.setreg(string.lower(v.value), v.content)
                      vim.notify("register: [" .. v.value .. "] cleared")
                    end
                  end
                end
              },
            }
          },
        },

        extensions = {
          extensions = {
            zoxide = {
              prompt_title = "[ Walking on the shoulders of TJ ]",
              mappings = {
                ["<C-s>"] = {
                  before_action = function(selection) print("before C-s") end,
                  action = function(selection)
                    vim.cmd.edit(selection.path)
                  end
                },
                ["<C-q>"] = { action = z_utils.create_basic_command("split") },
                ["<C-z>"] = { action = function(selection)
                  local o = vim.fn.system({ "zoxide", "remove", selection.path })
                  vim.notify("zoxide remove: " .. selection.path .. " with result: " .. o)
                end },
                ["<C-x>"] = { action = function(selection)
                  local o = vim.fn.system({ "zoxide", "remove", selection.path })
                  vim.notify("zoxide remove: " .. selection.path .. " with result: " .. o)
                end },
              },
            },
          },
          file_browser = {
            -- disables netrw and use telescope-file-browser in its place
            hijack_netrw = true,
            hidden = { file_browser = true, folder_browser = true },
            mappings = {
              ["i"] = {
                ["<C-t>"] = function(prompt_bufnr)
                  fb_actions.change_cwd(prompt_bufnr)

                  local current_picker = action_state.get_current_picker(prompt_bufnr)
                  local finder = current_picker.finder
                  local path = get_target_dir(finder)
                  local o = vim.fn.system({ "zoxide", "add", path })
                  vim.notify("zoxide add: " .. path .. " with result: " .. o)
                end,
                ["<C-z>"] = function(prompt_bufnr)
                  local current_picker = action_state.get_current_picker(prompt_bufnr)
                  local finder = current_picker.finder
                  local path = get_target_dir(finder)
                  local o = vim.fn.system({ "zoxide", "remove", path })
                  vim.notify("zoxide remove: " .. path .. " with result: " .. o)
                end,
                ["<C-x>"] = fb_actions.remove,
              },
              ["n"] = {
                ["<C-t>"] = function(prompt_bufnr)
                  fb_actions.change_cwd(prompt_bufnr)

                  local current_picker = action_state.get_current_picker(prompt_bufnr)
                  local finder = current_picker.finder
                  local path = get_target_dir(finder)
                  local o = vim.fn.system({ "zoxide", "add", path })
                  vim.notify("zoxide add: " .. path .. " with result: " .. o)
                end,
              },
            },
          },
          ["ui-select"] = {},
        },
      }

      -- Enable telescope fzf native, if installed
      pcall(require('telescope').load_extension, 'fzf')
      pcall(require('telescope').load_extension, 'file_browser')
      pcall(require('telescope').load_extension, 'zoxide')

      -- Add a mapping
      vim.keymap.set("n", "<leader>z", require('telescope').extensions.zoxide.list, { desc = 'change [d]irectory' })

      -- See `:help telescope.builtin`
      vim.keymap.set('n', '<leader>T', function()
        require('telescope.builtin').builtin { include_extensions = true }
      end, { desc = '[T]elescope' })
      vim.keymap.set('n', '<leader>?', require('telescope.builtin').oldfiles, { desc = '[?] Find recently opened files' })
      vim.keymap.set('n', '<leader><space>', require('telescope.builtin').buffers, { desc = '[ ] Find existing buffers' })
      vim.keymap.set('n', '<leader>/', function()
        -- You can pass additional configuration to telescope to change theme, layout, etc.
        require('telescope.builtin').current_buffer_fuzzy_find(require('telescope.themes').get_dropdown {
          winblend = 10,
          previewer = false,
        })
      end, { desc = '[/] Fuzzily search in current buffer' })

      vim.keymap.set('n', '<leader>gf', require('telescope.builtin').git_files, { desc = 'Search [G]it [F]iles' })
      vim.keymap.set('n', '<leader>sf', require('telescope.builtin').find_files, { desc = '[S]earch [F]iles' })
      vim.keymap.set('n', '<leader>sh', require('telescope.builtin').help_tags, { desc = '[S]earch [H]elp' })
      vim.keymap.set('n', '<leader>sw', require('telescope.builtin').grep_string, { desc = '[S]earch current [W]ord' })
      vim.keymap.set('n', '<leader>sg', require('telescope.builtin').live_grep, { desc = '[S]earch by [G]rep' })
      vim.keymap.set('n', '<leader>sd', require('telescope.builtin').diagnostics, { desc = '[S]earch [D]iagnostics' })

      vim.keymap.set('n', 'gr', require('telescope.builtin').lsp_references, { desc = '[G]oto [R]eferences' })
      vim.keymap.set('n', '<leader>ds', require('telescope.builtin').lsp_document_symbols,
        { desc = '[D]ocument [S]ymbols' })
      vim.keymap.set('n', '<leader>ws', require('telescope.builtin').lsp_dynamic_workspace_symbols,
        { desc = '[W]orkspace [S]ymbols' })
    end
  },
}
