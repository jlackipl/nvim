local feedkeys = vim.api.nvim_feedkeys
local termCode = vim.api.nvim_replace_termcodes

local function init()
	local toggleTerm = function()
		require("toggleterm").toggle(vim.v.count1)
	end

	vim.keymap.set('n', '<leader>\\', toggleTerm, { desc = 'TT: Open a [t]erminal' })
	vim.keymap.set('t', '<C-e>', "<C-\\><C-N>", { desc = 'ESC - Close terminal' })

	local function sstt()
		local tt = require("toggleterm")
		tt.send_lines_to_terminal("visual_selection", true, { args = vim.v.count1 })

		feedkeys(termCode('<C-e>', true, false, true), 'v', true)
	end

	local function ssltt()
		local tt = require("toggleterm")
		tt.send_lines_to_terminal("single_line", true, { args = vim.v.count1 })

		feedkeys(termCode('<C-e>', true, false, true), 'v', true)
	end

	vim.keymap.set('v', '<leader>S', sstt, { desc = 'TT: Send selection to terminal' })
	vim.keymap.set("v", "<leader>s", ssltt, { desc = "TT: send [s]ingle line to terminal" })
	-- Replace with these for the other two options
	-- require("toggleterm").send_lines_to_terminal("visual_lines", trim_spaces, { args = vim.v.count })
	-- require("toggleterm").send_lines_to_terminal("visual_selection", trim_spaces, { args = vim.v.count })

	local function tlg()
		local Terminal = require('toggleterm.terminal').Terminal
		local lazygit = Terminal:new({
			cmd = "lazygit",
			display_name = "LazyGit",
			direction = "float",
			hidden = true,
			float_opts = {
				border = "double",
			},

			close_on_exit = true,
			-- function to run on opening the terminal
			on_open = function(term)
				vim.cmd "startinsert!"
				vim.api.nvim_buf_set_keymap(term.bufnr, "n", "q", "<cmd>close<CR>",
					{ noremap = true, silent = true })
			end,
			on_close = function(_) end,
			count = 99,
		})

		lazygit:toggle()
	end

	vim.keymap.set("n", "<leader>gg", tlg, { desc = "[G]o [G]it", noremap = true, silent = true })

	local function tk()
		local Terminal = require('toggleterm.terminal').Terminal
		local lazygit = Terminal:new({
			cmd = "k9s",
			display_name = "K9s",
			direction = "float",
			hidden = true,
			float_opts = {
				border = "double",
			},

			close_on_exit = true,
			-- function to run on opening the terminal
			on_open = function(term)
				vim.cmd "startinsert!"
				vim.api.nvim_buf_set_keymap(term.bufnr, "n", "q", "<cmd>close<CR>",
					{ noremap = true, silent = true })
			end,
			on_close = function(_) end,
			count = 98,
		})

		lazygit:toggle()
	end

	vim.keymap.set("n", "<leader>gk", tk, { desc = "[G]o [k]9s", noremap = true, silent = true })
end

return {
	{
		"akinsho/toggleterm.nvim",
		lazy = false,
		version = "*",
		config = true,
		init = init,
	},
}
